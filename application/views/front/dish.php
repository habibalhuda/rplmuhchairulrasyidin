<div class="container p-4">
      <div class="row welcome text-center welcome">
            <div class="col-12">
                  <h1 class="display-4">Menu <?php echo $res['name']; ?></h1>
            </div>
      </div>
      <div class="container res-card">
            <div class="card">
                  <?php $img = $res['img'];?>
                  <img src="<?php echo base_url() . 'public/uploads/restaurant/thumb/' . $img; ?>"
                        class="card-img-top" />
                  <div class="card-body">
                        <h4 class="card-title font-weight-bold text-primary"><?php echo $res['name']; ?></h4>
                        <p class="card-text lead"><?php echo $res['address']; ?></p>
                        <p class="card-text"><?php echo $res['url']; ?></p>
                        <a href='https://api.whatsapp.com/send?phone=6285237330955' 
                                    class="btn btn-orange">Pesan Wisata</a>
                  </div>
            </div>
      </div>
</div>
<div class="container p-4 dish-card">
      <div class="row">
            <?php if (!empty($dishesh)) {?>
            <?php foreach ($dishesh as $dish) {?>
            <div class="col-md-6 col-lg-4 d-flex align-items-stretch">
                  <div class="card mb-4 shadow-sm">
                        
                  </div>
            </div>
            <?php }?>
            <?php } else {?>
            <div class="jumbotron">
                  <h1>No records found</h1>
            </div>
            <?php }?>
      </div>
      <hr class="my-4">
</div>